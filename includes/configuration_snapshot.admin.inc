<?php
/**
 * @file
 * Contains the callback fucntion that builds the admin form.
 */

/**
 * Page callback.
 */
function configuration_snapshot_admin_settings_form($form, &$form_state) {
  $form = array(
    'configuration_snapshot_config_path' => array(
      '#title' => t('Snapshot Configuration Path'),
      '#default_value' => variable_get('configuration_snapshot_config_path', conf_path() . '/files/configuration_snapshot'),
      '#description' => t('The file path of where the snapshot will be saved.'),
      '#type' => 'textfield',
    ),
  );

   return system_settings_form($form);
}
