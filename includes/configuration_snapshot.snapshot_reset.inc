<?php 
/**
 * @file
 * Contains the callback function that builds the admin form.
 */

/**
 * Page callback.
 */
function configuration_snapshot_admin_reset_to_snapshot_form($form, $form_state) {
  $form = array(
    'reset_snap_shot_title' => array(
      '#markup' => '<h3>Reset to snapshot.</h3>',
    ),
    'reset_snap_shot_message' => array(
      '#markup' => '<p>This will reset the schema and you will have to run the database updates.</p>',
    ),
  );

  return confirm_form($form, 'Are you sure?', 'admin/config/system/configuration-snapshot');
}

/**
 * Submit handler for reset to snapshot form.
 */
function configuration_snapshot_admin_reset_to_snapshot_form_submit($form, &$form_state) {
  db_update('system')
    ->fields(array('schema_version' => 0))
    ->condition('name', 'configuration_snapshot', '=')
    ->execute();

  $form_state['redirect'] = 'update.php';
}