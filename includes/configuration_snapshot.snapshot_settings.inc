<?php
/**
 * @file
 * Contains the callback fucntion that builds the admin form.
 */

/**
 * Page callback.
 */
function configuration_snapshot_admin_snapshot_settings_form($form, &$form_state) {
  $snapshot = configuration_snapshot_get_snapshot();
  $modules = _system_rebuild_module_data();

  $form = array(
    '#action' => '#', 
    '#tree' => TRUE,

    'modules' => array(
      '#type' => 'select',
      '#title' => t('Enabled Modules'),
      '#multiple' => TRUE,
      '#default_value' => array(),
      '#description' => t('Select the modules that will be enabled in the snapshot.'),
      '#options' => array(),
    ),

    'variable_settings_fieldset' => array(
      '#type' => 'fieldset',
      '#title' => t('Variable Settings'),
      '#prefix' => '<div id="variable-settings-fieldset-wrapper">',
      '#suffix' => '</div>',
      'add_rule' => array(
        '#type' => 'submit',
        '#value' => t('Add a variable setting.'),
        '#submit' => array('configuration_snapshot_admin_add_one_variable_setting'),
        '#ajax' => array(
          'callback' => 'configuration_snapshot_admin_add_variable_setting_callback',
          'wrapper' => 'variable-settings-fieldset-wrapper',
        )
      )
    ),

    'submit' => array( 
      '#type' => 'submit',
      '#value' => t('Save')
    ),
  );

  if ($modules) : 
    foreach ($modules as $module_name => $module) :
      if (!isset($form['modules']['#options'][$module->info['package']])) : 
        $form['modules']['#options'][$module->info['package']] = array();
      endif;

      $form['modules']['#options'][$module->info['package']][$module_name] = $module->info['name'];
    endforeach;

    if ($snapshot && isset($snapshot['700x']) && isset($snapshot['700x']['enabled_modules'])) :
      foreach ($snapshot['700x']['enabled_modules'] as $key => $enabled_module_name) :
        $form['modules']['#default_value'][] = $enabled_module_name;
      endforeach;
    endif;
  endif;

  if ($snapshot && isset($snapshot['700x']) && isset($snapshot['700x']['variables']) && !empty($snapshot['700x']['variables'])) :
    $snaphot_variables = true;

    $snapshot_vars_keys = array_keys($snapshot['700x']['variables']);
  else :
    $snaphot_variables = false;
  endif;

  if (empty($form_state['number_of_variable_settings'])) :
    if($snaphot_variables && count($snapshot['700x']['variables']) > 1): 
      $form_state['number_of_variable_settings'] = count($snapshot['700x']['variables']);
    else: 
      $form_state['number_of_variable_settings'] = 1;
    endif;
  endif;

  if ($form_state['number_of_variable_settings'] > 1) :
    $form['variable_settings_fieldset']['remove_rule'] = array(
      '#type' => 'submit',
      '#value' => t('Remove last variable setting.'),
      '#submit' => array('configuration_snapshot_admin_remove_one_variable_setting'),
      '#ajax' => array(
        'callback' => 'configuration_snapshot_admin_add_variable_setting_callback',
        'wrapper' => 'variable-settings-fieldset-wrapper',
      ),
    );
  endif;

  for ($i=0; $i < $form_state['number_of_variable_settings']; $i++) :
    if ($snaphot_variables && isset($snapshot['700x']['variables'][$snapshot_vars_keys[$i]]) && !empty($snapshot['700x']['variables'][$snapshot_vars_keys[$i]])) :
      $variable_name = $snapshot_vars_keys[$i];
      $variable_value = $snapshot['700x']['variables'][$snapshot_vars_keys[$i]];
    else :
      $variable_name = '';
      $variable_value = '';
    endif;

    $form['variable_settings_fieldset'][$i] = array(
      '#prefix' => '<div>',
      '#suffix' => '</div><br>',

      'variable_name' => array( 
        '#type' => 'textfield',
        '#title' => t('Variable Name'),
        '#description' => t('Enter the variable name.'),
        '#default_value' => $variable_name
      ),

      'variable_value' => array( 
        '#type' => 'textfield',
        '#title' => t('Variable Value'),
        '#description' => t('Enter the variable value.'),
        '#default_value' => $variable_value
      ),
    );
  endfor;

  return $form;
}

/**
 * Callback for both ajax-enabled buttons.
 *
 * Selects and returns the fieldset with the names in it.
 */
function configuration_snapshot_admin_add_variable_setting_callback($form, $form_state) {
  return $form['variable_settings_fieldset'];
}

/**
 * Submit handler for the "add-one-more" button.
 *
 * Increments the max counter and causes a rebuild.
 */
function configuration_snapshot_admin_add_one_variable_setting($form, &$form_state) {
  $form_state['number_of_variable_settings']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "remove one" button.
 *
 * Decrements the max counter and causes a form rebuild.
 */
function configuration_snapshot_admin_remove_one_variable_setting($form, &$form_state) {
  if ($form_state['number_of_variable_settings'] > 1) :
    $form_state['number_of_variable_settings']--;
  endif;

  $form_state['rebuild'] = TRUE;
}

/**
 * Callback function that saves form data.
 */
function configuration_snapshot_admin_snapshot_settings_form_submit($form, &$form_state) {
  $snapshot_template = configuration_snapshot_get_snapshot_template();

  foreach ($form_state['values']['modules'] as $module_name => $module) :
    $snapshot_template['700x']['enabled_modules'][] = $module_name;
  endforeach;

  foreach ($form_state['values']['variable_settings_fieldset'] as $key => $variable_fieldset) :
    if (!is_integer($key)) :
      continue;
    elseif ($key > 0 && (empty($variable_fieldset['variable_name']) || empty($variable_fieldset['variable_value']))) :
      drupal_set_message('The variable name and variable value cannot be empty.', 'error');
      continue;
    endif;

    $snapshot_template['700x']['variables'][$variable_fieldset['variable_name']] = $variable_fieldset['variable_value'];
  endforeach;
    
  $snapshot_saved = configuration_snapshot_save_snapshot($snapshot_template);

  if ($snapshot_saved): 
    drupal_set_message(t('The configuration snapshot has been saved.'));
  endif;
}
